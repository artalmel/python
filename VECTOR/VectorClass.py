from math import *
from tkinter import *
import os

class VecMan:
    def __init__(self,path):
        self.__path=path
        self.__createVector()
        self.__data=None

    def Save(self,*vector):
        f=open(self.__path,'w')
        d=dict()
        for i,j in enumerate(vector):
            d[str(i)]=j
            t=str(d)
        f.write(t)

    def Buffer(self,*vector):
        d=dict()
        for i,j in enumerate(vector):
            k='vector{0}'.format(i)
            d[k]=j
            t=str(d)
        return t

    def load(self,index):
        try:
            f=open(self.__path,'r')
            line=f.read()
            k=eval(str(line))
            self.__data=k.get(str(index))
            return self.__data
        except:
            return False

    def getVal(self,index):
        try:
            t=str(self.__data)
            a=t.replace('(','')
            b=a.replace(',','')
            c=b.replace(')','')
            k=c.split(' ')
            val=k[index]
            return val
        except:
            return False

    def __createVector(self):
        if os.path.exists(self.__path):
            return
        else:
            open(self.__path,'w')

class Vector:
    index=0

    def __init__(self,x=None,y=None,z=None):
        self.__data=None
        self.__i=0
        self.__length=0
        self.__vectorName=None
        Vector.index+=1
        if x!=None and y!=None and z!=None:
            self.__ReadFromInput(x,y,z)
        elif x!=None and y==None and z==None:
            self.__ReadFromFile(x)
        else:
            return None

    def __ReadFromFile(self,path):
        self.__data=VecMan(path)
        self.__data.load(self.Index())
        self.__x=float(self.__data.getVal(0))
        self.__y=float(self.__data.getVal(1))
        self.__z=float(self.__data.getVal(2))
        self.__Length()

    def __ReadFromInput(self,x,y,z):
        self.__x=float(x)
        self.__y=float(y)
        self.__z=float(z)
        self.__Length()


    def __Length(self):
        try:
            self.__length=sqrt((self.__x**2)+(self.__y**2)+(self.__z**2))
        except:
            return self

    def __str__(self):
        k=self.__x,self.__y,self.__z
        return str(k)

    def Name(self):
        self.__vectorName='vector',self.Index()
        return self.__vectorName

    def Index(self):
        self.__i=Vector.index
        return self.__i-1

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    def getZ(self):
        return self.__z

    def __add__(self,vector):
        if type(vector)==Vector:
            x=self.__x+vector.getX()
            y=self.__y+vector.getY()
            z=self.__z+vector.getZ()
            return Vector(x,y,z)


    def __sub__(self,vector):
        try:
            if type(vector)==Vector:
                x=self.__x-vector.getX()
                y=self.__y-vector.getY()
                z=self.__z-vector.getZ()
                return Vector(x,y,z)
        except:
            return self


    def __mul__(self,number):
        try:
            x=self.__x*float(number)
            y=self.__y*float(number)
            z=self.__z*float(number)
            return Vector(x,y,z)
        except:
            return self

    def __rmul__(self, number):
        try:
            x=float(number)*self.__x
            y=float(number)*self.__y
            z=float(number)*self.__z
            return (x,y,z)
        except:
            return self

    def scalar(self,vector):
        try:
            scalar=self.__x*vector.getX()+self.__y*vector.getY()+self.__z*vector.getZ()
            return scalar
        except:
            return self

    def __pow__(self, vector):
            y=(self.__x*vector.getZ())-(self.__z*vector.getX())
            x=(self.__y*vector.getZ())-(self.__z*vector.getY())
            z=(self.__x*vector.getY())-(self.__y*vector.getX())
            return Vector(x,y,z)


    def angleX(self):
        if self.__length!=0:
            cosX=acos(self.__x/self.__length)
            return degrees(cosX)

    def angleY(self):
        try:
            if self.__length!=0:
                return  self.__y/self.__length
        except:
            return 0

    def angleZ(self):
        try:
            if self.__length!=0:
                return  self.__z/self.__length
        except:
            return 0






'''
v1=Vector('aaa')

v2=Vector(10,20,30)
v3=Vector(5,6,8)
print(v1)
print(v2)
print(v3)
print('{0}+{1}={2}'.format(v2,v1,(v2+v1)))
print(v2-v1)
print(v2*10)
print(v2.scalar(v1))
print(v2.angleX())
print(v2**v3)
t=VecMan('aaa')
print(t.Buffer(str(v1),str(v2),str(v3),str((v2+v1))))
'''
#t.Save(str(v2),str(v3),str(v1))





