from tkinter import *
from  tkinter.messagebox import *
from tkinter.filedialog import *
from tkinter.colorchooser import *
from tkinter import ttk
import tkinter.font
import fileinput


class Application:
    def __init__(self,master):
        self.master=master
        self.master.title('Text Editor')
        self.master.geometry('600x450')
        self.master.maxsize(width=600,height=450)
        self.master.minsize(width=600,height=450)
        self.p=None #temp path
        self.seltext=None #temp text

        #menu
        m=Menu(self.master)
        root.config(menu=m)

        #file menu
        fm=Menu(m)
        m.add_cascade(label='File',menu=fm)
        fm.add_command(label='New',command=self.new)
        fm.add_command(label='Open',command=self.open)
        fm.add_separator()
        fm.add_command(label='Save',command=self.save)
        fm.add_command(label='SaveAs',command=self.saveAs)
        fm.add_separator()
        fm.add_command(label='Close',command=self.close)

        #edit menu
        em=Menu(m)
        m.add_cascade(label='Edit',menu=em)
        self.m=em
        em.add_command(label='Cut',command=self.cut)
        em.add_command(label='Copy',command=self.copy)
        em.add_command(label='Paste',command=self.paste)
        em.add_separator()
        em.add_command(label='Select ALL',command=self.selectAll)
        em.add_separator()
        em.add_command(label='Delete',command=self.delete)

        am=Menu(m)
        m.add_cascade(label='Help',menu=am)
        self.m=am
        am.add_command(label='About',command=self.About)


        #top frame
        topFrame=Frame(self.master,bd=2,relief=RAISED)
        topFrame.pack(fill=BOTH,side=TOP,expand=YES)

        #Left
        btnLeft=Button(topFrame,width=2,command=self.left)
        photo=PhotoImage(file='Images/msword_but_left.gif')
        btnLeft.config(image=photo,width="20",height="20")
        btnLeft.grid(row=0,column=6)

        #Center
        btnCenter=Button(topFrame,width=2,command=self.center)
        photo1=PhotoImage(file='Images/msword_but_cen.gif')
        btnCenter.config(image=photo1,width="20",height="20")
        btnCenter.grid(row=0,column=7)
        
        #Right
        btnRight=Button(topFrame,width=2,command=self.right)
        photo2=PhotoImage(file='Images/msword_but_rt.gif')
        btnRight.config(image=photo2,width="20",height="20")
        btnRight.grid(row=0,column=8)

        #Bold
        btnBold=Button(topFrame,width=2,command=self.bold)
        photo3=PhotoImage(file='Images/msword_but_bold.gif')
        btnBold.config(image=photo3,width="20",height="20")
        btnBold.grid(row=0,column=9)
        
        #Italic
        btnItal=Button(topFrame,width=2,command=self.ital)
        photo4=PhotoImage(file='Images/msword_but_ital.gif')
        btnItal.config(image=photo4,width="20",height="20")
        btnItal.grid(row=0,column=10)

        #Underline
        btnUnder=Button(topFrame,width=2,command=self.under)
        photo5=PhotoImage(file='Images/msword_but_und.gif')
        btnUnder.config(image=photo5,width="20",height="20")
        btnUnder.grid(row=0,column=11)
        
        #New
        btnNew=Button(topFrame,width=2,command=self.new)
        photo8=PhotoImage(file='Images/word_but_new.gif')
        btnNew.config(image=photo8,width="20",height="20")
        btnNew.grid(row=0,column=0)

        #Open
        btnOpen=Button(topFrame,width=2,command=self.open)
        photo7=PhotoImage(file='Images/word_but_open.gif')
        btnOpen.config(image=photo7,width="20",height="20")
        btnOpen.grid(row=0,column=1)

        #Save
        btnSave=Button(topFrame,width=2,command=self.save)
        photo6=PhotoImage(file='Images/word_but_save.gif')
        btnSave.config(image=photo6,width="20",height="20")
        btnSave.grid(row=0,column=2)

        #Cut
        btnCat=Button(topFrame,width=2,command=self.cut)
        photo9=PhotoImage(file='Images/word_but_cut.gif')
        btnCat.config(image=photo9,width="20",height="20")
        btnCat.grid(row=0,column=3)

        #Copy
        btnCopy=Button(topFrame,width=2,command=self.copy)
        photo10=PhotoImage(file='Images/word_but_copy.gif')
        btnCopy.config(image=photo10,width="20",height="20")
        btnCopy.grid(row=0,column=4)

        #Paste
        btnPaste=Button(topFrame,width=2,command=self.paste)
        photo11=PhotoImage(file='Images/word_but_paste.gif')
        btnPaste.config(image=photo11,width="20",height="20")
        btnPaste.grid(row=0,column=5)

        #Text color
        self.btnTxtColor=Button(topFrame,text='TC',width='1',command=self.textColor,font=('times',12,'bold'))
        self.btnTxtColor.grid(row=0,column=12)

        #Bgcolor
        self.btnColor=Button(topFrame,text='BgC',width='1',command=self.bgColor,font=('times',12,'bold'))
        self.btnColor.grid(row=0,column=13)

        #bottom frame
        bottomFrame=Frame(self.master,bd=2,relief=RAISED)
        bottomFrame.pack(fill=BOTH,side=BOTTOM, expand=YES)

        #text area
        self.txt=Text(bottomFrame,wrap=WORD)
        scr = Scrollbar(bottomFrame,command=self.txt.yview)
        self.txt.configure(yscrollcommand=scr.set)
        self.txt.pack(fill=BOTH,side=LEFT,expand=YES)
        scr.pack(fill=Y,side=RIGHT)

        #mouse right_Click event
        self.master.bind("<Button-3>", self.contextMenu)
        self.master.mainloop()


    def new(self):
        self.p=''
        self.txt.delete(1.0,END)

    def saveAs(self):
        try:
            sa=asksaveasfilename()#save filedialog
            self.p=sa
            t=self.txt.get(1.0,END)
            f=open(sa,'w')
            f.write(t)
            f.close()
        except FileNotFoundError:
            return

    def save(self):
        try:
            t=self.txt.get(1.0,END)
            f=open(self.p,'w')
            f.write(t)
            f.close()
        except:
            self.saveAs()

    def open(self):
        try:
            self.p=''#clear temp path
            op=askopenfilename()#open filedialog
            self.p=op
            self.txt.delete(1.0,END)
            for l in fileinput.input(op):
                self.txt.insert(END,l)
        except :
            showwarning("Open file",
            "Cannot open file")

    def close(self):
        self.master.destroy()

    def cut(self):
        try:
            self.seltext=self.txt.selection_get()
            start = self.txt.index("sel.first")#selection start index
            end = self.txt.index("sel.last")#selection end index
            self.txt.delete(start, end)
        except TclError:
            showerror('Error','Empty string')

    def paste(self):
        try:
            self.txt.insert(INSERT,self.seltext)
        except TclError:
            showerror('Empty memory','Memory is empty')

    def copy(self):
        try:
            self.seltext=self.txt.selection_get()
        except:
            showerror('Error','Empty string')

    def selectAll(self):
        self.txt.tag_add('sel',1.0,END)

    def delete(self):
        try:
            start = self.txt.index("sel.first")
            end = self.txt.index("sel.last")
            self.txt.delete(start, end)
        except TclError:
            showerror('Error','Empty string')

    def contextMenu(self,event):
        self.m.post(event.x_root, event.y_root)

    def left(self):
        self.txt.tag_remove('center','insert linestart','insert lineend')
        self.txt.tag_remove('right','insert linestart','insert lineend')
        try:
            self.txt.tag_add('s1','sel.first','sel.last')
            self.txt.tag_config('s1',justify=LEFT)
            self.txt.tag_delete('s','s2')
        except:
            self.txt.tag_delete('s','s1','s2')
            self.txt.tag_add('left','insert linestart','insert lineend')
            self.txt.tag_config('left',justify=LEFT)

    def right(self):
        self.txt.tag_remove('left','insert linestart','insert lineend')
        self.txt.tag_remove('center','insert linestart','insert lineend')
        try:
            self.txt.tag_add('s','sel.first','sel.last')
            self.txt.tag_config('s',justify=RIGHT)
            self.txt.tag_delete('s1','s2')
        except:
            self.txt.tag_delete('s','s1','s2')
            self.txt.tag_add('right','insert linestart','insert lineend')
            self.txt.tag_config('right',justify=RIGHT)

    def center(self):
         self.txt.tag_remove('left','insert linestart','insert lineend')
         self.txt.tag_remove('right','insert linestart','insert lineend')
         try:
             self.txt.tag_add('s2','sel.first','sel.last')
             self.txt.tag_config('s2',justify=CENTER)
             self.txt.tag_delete('s1','s')
         except:
             self.txt.tag_delete('s','s1','s2')
             self.txt.tag_add('center','insert linestart','insert lineend')
             self.txt.tag_config('center',justify=CENTER)

    def bold(self):
        try:
            self.txt.tag_delete('italic','under')
            self.start_end('bold')
            self.txt.tag_config('bold',font=('times',12,'bold'))
        except:
            return

    def ital(self):
        try:
            self.txt.tag_delete('bold','under')
            self.start_end('italic')
            self.txt.tag_config('italic',font=('times',12,'italic'))
        except:
            return 

    def under(self):
        try:
            self.txt.tag_delete('italic','bold')
            self.start_end('under')
            self.txt.tag_config('under',font=('times',12,'underline'))
        except:
            return

     #color

    def textColor(self):
        try:
            color=askcolor()
            color_name=color[1]
            start = self.txt.index("sel.first")
            end = self.txt.index("sel.last")
            self.txt.tag_add("color", start, end)
            self.txt.tag_config("color",foreground=color_name)
            self.btnTxtColor.configure(fg=color_name)
        except:
            return

    def bgColor(self):
        try:
            color=askcolor()
            color_name=color[1]
            start = self.txt.index("sel.first")
            end = self.txt.index("sel.last")
            self.txt.tag_add("colorbg", start, end)
            self.txt.tag_config("colorbg",background=color_name)
            self.btnColor.configure(bg=color_name)
        except:
            return

    def About(self):
        print('About')
        abwindow=Toplevel(self.master)
        abwindow.title('About')
        abwindow.geometry('200x100')
        abwindow.maxsize(width=200,height=100)
        abwindow.minsize(width=200,height=100)
        abLabel1=Label(abwindow,text='Text Editor')
        abLabel1.config(font=('times',12,'bold'),justify=CENTER)
        abLabel1.pack()
        abLabel2=Label(abwindow,text='Author: Artem\n Aleksanyan')
        abLabel2.config(font=('times',12,'italic'),justify=CENTER)
        abLabel2.pack()

    def start_end(self,tag_name):
        start=self.txt.index('sel.first')
        end=self.txt.index('sel.last')
        self.txt.tag_add(tag_name,start,end)

root=Tk()
Application(root)

